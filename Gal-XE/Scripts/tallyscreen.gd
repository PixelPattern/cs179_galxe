extends CanvasLayer

onready var ap = get_node("AnimationPlayer")
onready var label = get_node("RichTextLabel")
onready var timer = get_node("Timer")
onready var contin= get_node("Label")

var ready = false
var ready2 = false
var ready3 = false

var player = 0
var middlescore = 0
var topscore = 0
var curr = 0

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func _process(delta):
	
	#if Input.is_action_just_pressed("ui_up"):
	#	play_tally(100000, 50000, 100000)
	#pass
	
	if contin.visible == true && Input.is_action_just_pressed("all"):
		var tree = get_tree()
		tree.paused = false
		get_tree().change_scene("res://Menus/TitleMenu.tscn")
		pass
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func play_tally(ps, ms, ts):
	
	topscore = ts
	middlescore = ms
	player = ps
	var tree = get_tree()
	tree.paused = true
	ap.play("startplaying")
	timer.start()
	ready = true
	pass


func _on_Timer_timeout():
	if curr < player:
		curr += 223
		label.set_bbcode("[center]" + str(curr) + "[/center]")
		
	
	if curr >= player:
		curr = player
		label.set_bbcode("[center]" + str(curr) + "[/center]")
		contin.visible = true
	
	if ready == true && ready2 == false && curr >= middlescore:
		ap.play("secondplace")
		ready2 = true
		pass
	
	if ready == true && ready3 == false && curr >= topscore:
		ap.play("firstplace")
		ready3 = true
		pass
	
	pass # replace with function body
