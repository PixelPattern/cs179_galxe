extends "res://Scripts/EnemyKB.gd"

# variables for randomized movement
export (float, 5) var _MAX_SPEED = 1
export (float, 5) var _MAX_RAND_TM = 1.0
export (float, 1, 100) var _MAX_BOUNCE_POW = 50
var _cur_max_tm = 0.0
var _rand_tm = 1e20
var _rand_dir = Vector2 (0, 0)
var _attack_dir = Vector2 (0, 0)
var _facing_rgt = false
var out_of_bnds = false

# flag for when the chaser attacks the player
var attack_mode = false

var prev_state = T.Enemy.Hit.NONE

# reference to animation player
onready var anim_player = get_node("ChaserAnim")
onready var spawn = get_parent()

# chaser setter for _mvmt_vect
# sets the movement vector and the hit state
func mvmt_set(new_mvmt = _base_mvmt, state = T.Enemy.Hit.NONE):
	if state != T.Enemy.Hit.BOUNCE:
		if state == T.Enemy.Hit.FALL:
			if out_of_bnds:
				_cur_mvmt = new_mvmt * 3 * _MAX_SPEED
		elif state == T.Enemy.Hit.GRAPPLE:
			_cur_mvmt = new_mvmt
		# set the knockback timer to zero
		elif state == T.Enemy.Hit.PUNCH:
			_kback_tm = 0
	else:
		_bounce_tm = 0
		_cur_mvmt = new_mvmt * _MAX_SPEED * _MAX_BOUNCE_POW
	_hit_state = state
	pass

func _ready():
	_life_state = T.Enemy.Life.ALIVE
	_cur_health = _max_health
	_cur_mvmt = _base_mvmt
	_hit_state = T.Enemy.Hit.NONE
	anim_player.play("chaser_wings")
	self._random_mvmt()
	pass

func _default_move():
	if attack_mode:
		_cur_mvmt = _attack_dir * _MAX_SPEED
	else:
		_cur_mvmt = _rand_dir * _MAX_SPEED
	pass

func _attack():
	if !anim_player.is_playing():
		anim_player.play("chaser_attack")
	pass

# helper function to flip the chaser around so it faces the correct direction
# while moving
func _orientation_helper():
	var prev_dir = _facing_rgt
	# get the orientation of the chaser
	if _cur_mvmt.x < -0.05:
		_facing_rgt = false
	elif _cur_mvmt.x > 0.05:
		_facing_rgt = true
	# flip the chaser if facing new direction than before
	if _facing_rgt != prev_dir:
		self.scale.x = -1
	pass

func _physics_process(delta):
	# update the random movement
	#if override == false:
	_rand_tm += delta
	if _rand_tm >= _cur_max_tm:
		_rand_tm = 0
		self._random_mvmt()
	
	if _life_state == T.Enemy.Life.ALIVE:
		# Decide what to do based on the hit state of the enemy
		match _hit_state:
			T.Enemy.Hit.PUNCH:
				if _kback_tm <= _MAX_KBACK_TM:
					_kback_tm += delta
				else:
					_kback_tm = 1e20
					_hit_state = T.Enemy.Hit.NONE
			T.Enemy.Hit.GRAPPLE:
				_hit_state = T.Enemy.Hit.GRAPPLE
			T.Enemy.Hit.BOUNCE:
				if _bounce_tm <= _MAX_BOUNCE_TM:
					_bounce_tm += delta
				else:
					_bounce_tm = 1e20
					if not out_of_bnds:
						_hit_state = T.Enemy.Hit.NONE
					else:
						_hit_state = T.Enemy.Hit.FALL
			T.Enemy.Hit.FALL:
				if not out_of_bnds:
					_hit_state = T.Enemy.Hit.NONE
				else:
					# set the random movement to go back to the spawn node
					_cur_mvmt = self.get_parent().get_global_transform().get_origin() - self.get_global_transform().get_origin()
					_cur_mvmt = _cur_mvmt.normalized() * 3 * _MAX_SPEED
			_:
				if out_of_bnds:
					_hit_state = T.Enemy.Hit.FALL
				else:
					self._default_move()
		self._collision_manager()
	
	else:
		# notify spawn node that this chaser is dead
		if self.get_parent().has_method("update_status") and not self.is_queued_for_deletion():
			self.get_parent().update_status(self)
		self.queue_free()
	pass

# helper function to randomize chaser movement
func _random_mvmt():
	randomize()
	# set the movement timer
	_cur_max_tm = randf() * _MAX_RAND_TM
	_rand_tm = 0.0
	
	# set the direction
	_rand_dir.x = (2 * randf()) - 1
	_rand_dir.y = (2 * randf()) - 1
	_rand_dir.normalized()
	pass

func _collision_manager():
	self._orientation_helper()
	var collision = self.move_and_collide(_cur_mvmt)
	if collision:
		var col_obj = collision.collider
		
		# deal damage to player if enemy bumps into player
		# enemy must be moving into player for godot to register that
		# enemy bumped into player
		if col_obj.get_collision_layer_bit(T.Col_Layer.PLAYER):
			# update player health if object has the function to do so
			#self._attack()
			if col_obj.has_method("health_set"):
				col_obj.health_set(_attack_pow)
		
		# bounce off of the player and walls
		if col_obj.get_collision_layer_bit(T.Col_Layer.PLAYER) or col_obj.get_collision_layer_bit(T.Col_Layer.WALL):
			self._bounce_collision(collision)
	#._collision_manager()
	pass

# helper function to manage the bounce reaction of the chaser enemy
func _bounce_collision(collision):
	_hit_state = T.Enemy.Hit.BOUNCE
	# reset the various timers
	_kback_tm = 1e20
	_bounce_tm = 0
	
	_cur_mvmt = _cur_mvmt.bounce(collision.normal).normalized() * _MAX_SPEED * _MAX_BOUNCE_POW
	var reflect = collision.remainder.bounce(collision.normal)
	self.move_and_collide(reflect)
	pass

# helper function to set the attack mode flag for movement
func _set_attack_mode(var x):
	attack_mode = x
	pass

# helper function to set the attack vector for the chaser
func _set_attack_dir(var dir):
	_attack_dir = dir
	pass