extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var max_value;
var health_value;


onready var healthbar = get_node("ProgressBar");
 
func _ready():
   	# Called every time the node is added to the scene.
	# Initialization here
	max_value = get_parent().MAX_HEALTH;
	health_value = max_value;
	healthbar.set_value(health_value);
	pass

func _process(delta):
	"""if Input.is_action_just_pressed("DecreaseHB") and health_value >= 0:
		health_value = health_value - 10
		if health_value < 0:
			health_value = 0
		healthbar.set_value(health_value)
	if Input.is_action_just_pressed("IncreaseHB") and health_value <= max_value:
		health_value = health_value + 10
		if health_value > max_value:
			health_value = max_value
		healthbar.set_value(health_value)"""
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
	#health_value = health_value - 10;
	
	#if health_value >= min_value:
	#	healthbar.set_value(health_value);
	
	#else: 
	#	healthbar.set_value(0);
	
	pass


func _on_Player_health_change(cur_health):
	healthbar.set_value(cur_health)
	pass
