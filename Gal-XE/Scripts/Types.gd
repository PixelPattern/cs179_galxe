# enumerator for collision layers of various game objects
enum Col_Layer{
	DEFAULT = 0,
	PLAYER = 1,
	ENEMY = 2,
	WALL = 3,
	PLATFORM = 4
	CHASER = 5
}

# states of play the level can be in
enum Level_State{
	PLAY,
	END,
	GAME_OVER
}

# Class for enumerators for states of player character and fighter enemy fists
class Player:
	# enumerator to keep track of the states of the fists
	enum Action{
		NO_PUNCH,
		PAUSE_PUNCH,
		LFT_PUNCH,
		RGT_PUNCH,
		GRAB
	}
	
	# enumerator to keep track of the states of the input
	enum Input{
		READY,
		WAITING
	}

# Class for enumerators for various states of enemy characters
class Enemy:
	# enumerator to keep track of what type of collisions the enemy is undergoing
	enum Hit{
		PUNCH,
		GRAPPLE,
		BOUNCE,
		FALL,
		NONE
	}
	
	# enumerator to keep track of what type of movement the fighter enemy is doing
	enum Move{
		PULSE,
		DRIFT,
		BOUNCE,
		REACT
	}
	
	# enumerator to indicate if enemy is alive or dead
	enum Life{
		ALIVE,
		DEAD
	}