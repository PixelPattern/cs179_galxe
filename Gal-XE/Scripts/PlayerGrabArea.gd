extends Area2D

# container for various enumerators
const T = preload("res://Scripts/Types.gd")
# time variables
const _GRAB_TM = 0.2
var _cur_grab_tm = 1e20

# container for enemies that are grabbed
var grabbed_enemies = []

# reference to PlayerFists object
onready var _fists_node = self.get_node ("../../../../../")

# reference to FistCenter object
onready var _fist_center = self.get_node("../FistCenter")

# reference to previous state of fists in last frame
var _prev_punch_state

func _ready():
	self.connect("body_entered", self, "_enemy_grab")
	self.connect("body_exited", self, "_enemy_escape")
	if _fists_node.has_method("punch_state_get"):
		_prev_punch_state = _fists_node.punch_state_get()
	pass

func _process(delta):
	var cur_punch_state = T.Player.Action.NO_PUNCH
	if _fists_node.has_method("punch_state_get"):
		cur_punch_state = _fists_node.punch_state_get()
	
	# activate collisions when the grab state is active
	if cur_punch_state == T.Player.Action.GRAB:
		# check previous state to see if a new grab action has started
		# if so, activate collisions
		if _prev_punch_state != cur_punch_state:
			_cur_grab_tm = 0
			self.set_collision_mask_bit(T.Col_Layer.ENEMY, true)
			self.set_collision_mask_bit(T.Col_Layer.CHASER, true)
		# activate collisions while the grabbing portion of the grab animation
		# is being played
		elif _cur_grab_tm < _GRAB_TM:
			_cur_grab_tm += delta
			self.set_collision_mask_bit(T.Col_Layer.ENEMY, true)
			self.set_collision_mask_bit(T.Col_Layer.CHASER, true)
		# deactivate collisions when the grabbing part of the grab animation
		# has finished playing
		else:
			_cur_grab_tm = 1e20
			self._end_grab()
			self.set_collision_mask_bit(T.Col_Layer.ENEMY, false)
			self.set_collision_mask_bit(T.Col_Layer.CHASER, false)
	
	# deactivate collisions when the grab state is inert
	else:
		self.set_collision_mask_bit(T.Col_Layer.ENEMY, false)
		self.set_collision_mask_bit(T.Col_Layer.CHASER, false)
	
	_prev_punch_state = cur_punch_state
	self._move_enemy()
	pass

# sets grabbed enemy movement upon completeion of grabbing protion
# of grab animation
func _end_grab():
	var new_mvmt
	for enemy in grabbed_enemies:
		if _fists_node.get_parent().has_method("mvmt_get"):
			new_mvmt = _fists_node.get_parent().mvmt_get()
			if enemy.has_method("mvmt_set"):
				enemy.mvmt_set(new_mvmt, T.Enemy.Hit.FALL)
	grabbed_enemies.clear()
	pass

# updates movement for each enemy grabbed
func _move_enemy():
	# get global position of the center of the fist
	var grab_origin = _fist_center.get_global_transform().get_origin()
	var enemy_origin
	var grab_mvmt
	
	# move all grabbed enemies with the fist
	for enemy in grabbed_enemies:
		# get the global position of the enemy
		enemy_origin = enemy.get_global_transform().get_origin()
		
		# get the direction of the vector from the enemy to the fist center
		grab_mvmt = (grab_origin - enemy_origin).normalized() * 17
		
		# add player movement to enemy grab movement
		if _fists_node.get_parent().has_method("mvmt_get"):
			grab_mvmt += self._fists_node.get_parent().mvmt_get()
		
		if enemy.has_method("mvmt_set"):
			enemy.mvmt_set(grab_mvmt, T.Enemy.Hit.GRAPPLE)
	pass

# signal function to detect enemies hit by the grab
func _enemy_grab(enemy):
	if enemy.get_collision_layer_bit(T.Col_Layer.ENEMY) or enemy.get_collision_layer_bit(T.Col_Layer.CHASER):
		grabbed_enemies.append(enemy)
		# make player invisible to enemy
		enemy.set_collision_mask_bit(T.Col_Layer.PLAYER, false)
		# change non-chaser enemy flags
		if enemy.get_collision_layer_bit(T.Col_Layer.ENEMY):
			enemy.set_collision_layer_bit(T.Col_Layer.ENEMY, false)
		# change chaser enemy flags
		#else:
		#	enemy.set_collision_layer_bit(T.Col_Layer.CHASER, false)
		#	enemy.set_collision_mask_bit(T.Col_Layer.DEFAULT, true)
		self._move_enemy()
	pass

# signal function to detect enemies that escape grab
func _enemy_escape(enemy):
	# make player visible to enemy
	enemy.set_collision_mask_bit(T.Col_Layer.PLAYER, true)
	# enemy is a chaser
	#if enemy.get_collision_layer_bit(T.Col_Layer.DEFAULT):
	#	enemy.set_collision_layer_bit(T.Col_Layer.CHASER, true)
	# enemy is not a chaser
	if not enemy.get_collision_layer_bit(T.Col_Layer.CHASER):
		enemy.set_collision_layer_bit(T.Col_Layer.ENEMY, true)
	grabbed_enemies.erase(enemy)
	pass