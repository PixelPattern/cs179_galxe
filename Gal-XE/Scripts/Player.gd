extends KinematicBody2D

# container for various enumerators
const T = preload("res://Scripts/Types.gd")
onready var _anim_player = self.get_node("invul")
onready var pulse = get_node("pulse")
onready var hit = get_node("hit")
onready var pts = get_tree().get_root().get_node("Level1")
# signal for updating the pulse gui
signal pulse_change(p_cnt)

# signal for indicating a change in health
signal health_change(cur_health)

# signal for managing updates of points and multiplier
signal multiplier_reset

# player health
export (int, 1, 100) var MAX_HEALTH = 50
var _health = 0 setget health_set, health_get

# invulnerability flag
var invul = false setget ,is_invulnerable

# constant values
var _JSTICK_ZERO_M = 0.25
var _JSTICK_EDGE_M = 0.1
export (float, 1.0) var _JSTICK_FLIP_M = 0.15
export (int, 100) var _MAX_PULSE_CNT = 5
export (float, 2.0) var _MAX_PULSE_TM = 0.2
var _MAX_PULSE_TRIG_TM = 0.1
export (float, 1.0, 2000) var _PULSE_ACCEL = 1000
export (float, 20.0, 30.0) var _MAX_VEL = 20.0
export var _GRAVITY = 50

# movement variables
var _pulse_vect = Vector2 (0, 0)
var _drift_vect = Vector2 (0, 0)
var _pulse_cnt = 0
var _pulse_tm = 1e20
var _pulse_trig_tm = 1e20

# control input flag
var _jpad_connected = true

# joypad input variables
var _jpad_vect = Vector2 (0, 0)
var _jpad_index = 0

# joypad input flags
var _jpad_centered = false

# keyboard input flags
var _pulse_lft = false
var _pulse_rgt = false
var _pulse_up = false
var _pulse_dwn = false

# pulse state flags
var _pulsing = false
var _rcvd_input = false

# orientation flag
var _facing_rgt = true setget ,orientation_get

# setter for _health
# value to enter is the value to change the health by
# positive values decrease the health while negative values increase it
func health_set(decrease = 0):
	self._calc_health(decrease)
	pass

# getter for _health
func health_get():
	return _health
	pass

# getter for invulnerability
func is_invulnerable():
	return invul
	pass

# getter for orientation
func orientation_get():
	return _facing_rgt
	pass

# setter for movement vector
# should be called only when an enemy is altering the player's movement
func mvmt_set(new_mvmt, hit_state = T.Enemy.Hit.NONE):
	# reset the pulse vector if pulsing
	if _pulsing:
		_pulsing = false
		_rcvd_input = false
		_pulse_tm = 1e20
		_pulse_vect = Vector2(0, 0)
	# set the pulse count to one if no pulses remain
	if _pulse_cnt >= _MAX_PULSE_CNT:
		_pulse_cnt = _MAX_PULSE_CNT - 1
		self.emit_signal("pulse_change", _pulse_cnt)
	# set the drift vector to the new movement vector
	_drift_vect = new_mvmt
	pass

# getter for appropriate movement vector
func mvmt_get():
	# if pulsing the pulse vector will be greater than zero, so the pulse vector
	# is used for current movement
	if _pulse_vect.length() > 0:
		return _pulse_vect
	# if not pulsing the pulse vector is set to zero, so the drift vector is used
	# for current movement
	else:
		return _drift_vect
	pass

func _ready():
	# set up the connection to the joypad of applicable
	Input.connect("joy_connection_changed", self, "_on_joy_connection_changed")
	var connected_jpads = Input.get_connected_joypads()
	if connected_jpads.empty():
		_jpad_connected = false
	else:
		_jpad_connected = true
		_jpad_index = connected_jpads[0]
	
	# initialize player stats
	_health = MAX_HEALTH
	pass

func _physics_process(delta):
	#self._health_test()
	# turn off invulnerability when invulnerability animation has finished
	if !_anim_player.is_playing():
		invul = false
	
	# get joypad input values
	if _jpad_connected:
		_jpad_vect = Vector2(Input.get_joy_axis (_jpad_index, JOY_AXIS_0), Input.get_joy_axis(_jpad_index, JOY_AXIS_1))
		self._jpad_center_helper(delta)
	
	# get keyboard input values
	else:
		_pulse_lft = Input.is_action_just_pressed ("player_left")
		_pulse_rgt = Input.is_action_just_pressed ("player_right")
		_pulse_up = Input.is_action_just_pressed ("player_up")
		_pulse_dwn = Input.is_action_just_pressed ("player_down")
	
	# pulse
	if _pulsing and _pulse_tm < _MAX_PULSE_TM:
		var vel_vect = _pulse_vect * _PULSE_ACCEL
		self._collision_manager(vel_vect * delta)
		_pulse_tm += delta
	
	# deactivate pulse and prepare to recieve input
	elif _pulsing and _pulse_tm > _MAX_PULSE_TM:
		_drift_vect = _pulse_vect * _PULSE_ACCEL * delta
		_pulse_vect = Vector2 (0, 0)
		_pulse_tm = 1e20
		_pulsing = false
		_rcvd_input = false
	
	# check for pulse when pulses are available
	elif _pulse_cnt < _MAX_PULSE_CNT and not _rcvd_input:
		# handle joypad input
		if _jpad_connected:
			if _jpad_centered and (_jpad_vect.length() + _JSTICK_EDGE_M) >= 1.0:
				_pulse_vect = _jpad_vect.normalized()
				self._input_helper()
		
		# handle keyboard input
		else:
			if _pulse_lft or _pulse_rgt or _pulse_up or _pulse_dwn:
				if _pulse_lft and not _pulse_rgt:
					_pulse_vect.x = -1.0
				elif _pulse_rgt and not _pulse_lft:
					_pulse_vect.x = 1.0
				if _pulse_up and not _pulse_dwn:
					_pulse_vect.y = -1.0
				elif _pulse_dwn and not _pulse_up:
					_pulse_vect.y = 1.0
				_pulse_vect = _pulse_vect.normalized()
				self._input_helper()
		
		# use drift vector for collision calculation when pulse is not detected
		if not _pulsing:
			self._drift_mvmt_helper(delta)
	
	# drift when out of pulses
	else:
		self._drift_mvmt_helper(delta)
	pass

# helper function to orient the player based on the direction of the pulse
func _orientation_helper():
	var prev_dir = _facing_rgt
	# get the orientation of the player
	if _pulse_vect.x < -_JSTICK_FLIP_M:
		_facing_rgt = false
	elif _pulse_vect.x > _JSTICK_FLIP_M:
		_facing_rgt = true
	# flip the player if facing new direction than before
	if _facing_rgt != prev_dir:
		self.scale.x = -1
	pass

# helper function to set the appropriate flags and values upon recieving pulse input
func _input_helper():
	_pulsing = true
	_rcvd_input = true
	_pulse_tm = 0
	_pulse_cnt += 1
	pulse.play()
	self.emit_signal("pulse_change", _pulse_cnt)
	_pulse_trig_tm = 1e20
	self._orientation_helper()
	pass

# helper function to check if joystick is centered
# the player will not pulse if the joystick is not considered centered
#
# the joystick is considered centered if the joystick is centered
# or has been centered within _MAX_PULSE_TRIG_TM
func _jpad_center_helper(delta):
	if (_jpad_vect.length() - _JSTICK_ZERO_M) <= 0.0:
		_jpad_centered = true
		_pulse_trig_tm = 0
	else:
		if _pulse_trig_tm < _MAX_PULSE_TRIG_TM:
			_pulse_trig_tm += delta
		else:
			_jpad_centered = false
			_pulse_trig_tm = 1e20
	pass

# helper function to handle player movement after a pulse jump
func _drift_mvmt_helper(delta):
	if _drift_vect.length() < _MAX_VEL:
		_drift_vect.y += _GRAVITY * delta
	self._collision_manager(_drift_vect)
	pass

# helper function to handle collisions between different types of nodes
func _collision_manager(mvmt_vect):
	# get information about collision
	var collision = self.move_and_collide(mvmt_vect)
	
	if collision:
		var col_obj = collision.collider
		
		# amount to decrease the movement vector by upon bouncing off of something
		var bounce_decay
		
		# reset the pulse count after hiting a platform
		if col_obj.get_collision_layer_bit(T.Col_Layer.PLATFORM):
			_pulse_cnt = 0
			self.emit_signal("pulse_change", _pulse_cnt)
		
		# take damage from enemy if player bumps into enemy
		# player must be moving into enemy for godot to register that
		# player bumped into enemy
		if col_obj.get_collision_layer_bit(T.Col_Layer.ENEMY) or col_obj.get_collision_layer_bit(T.Col_Layer.CHASER):
			# take damage from enemy if enemy is not being punched
			if col_obj.has_method("hit_get"):
				#check to see if enemy is being punched
				if col_obj.hit_get() != T.Enemy.Hit.PUNCH:
					# update player health if able to get enemy's attack power
					if col_obj.has_method("attack_get"):
						self._calc_health(col_obj.attack_get())

			
			# Turn on invulnerability state
			#if !_anim_player.is_playing():
			#	_anim_player.play("invul")
			#	invul = true
		
		# bounce player off of object if object can be collided into
		if not col_obj.get_collision_layer_bit(T.Col_Layer.DEFAULT):
			var reflect = collision.remainder.bounce(collision.normal)
			
			# set bounce decay for bouncing off of enemies
			if col_obj.get_collision_layer_bit(T.Col_Layer.ENEMY) or col_obj.get_collision_layer_bit(T.Col_Layer.CHASER):
				# set movement of chaser enemy
				if col_obj.get_collision_layer_bit(T.Col_Layer.CHASER) and col_obj.has_method("mvmt_set"):
					col_obj.mvmt_set(collision.remainder.normalized(), T.Enemy.Hit.BOUNCE)
				# set high bounce decay if enemy is being punched
				if col_obj.has_method("hit_get"):
					if col_obj.hit_get() == T.Enemy.Hit.PUNCH:
						bounce_decay = 3
					# set low bounce decay if enemy is not being punched
					else:
						bounce_decay = 1.5
				# set low bounce decay if enemy is not being punched
				else:
					bounce_decay = 1.5
			# set default bounce decay
			else:
				bounce_decay = 3.0
			
			_drift_vect = mvmt_vect.bounce(collision.normal) / bounce_decay
			
			# reset pulse upon impact
			if _pulsing:
				_pulsing = false
				_rcvd_input = false
				_pulse_tm = 1e20
				_pulse_vect = Vector2(0, 0)
			else:
				self.move_and_collide(reflect)
	pass

#func _health_test():
#	if Input.is_action_just_pressed("DecreaseHB"):
#		_calc_health(10)
#	if Input.is_action_just_pressed("IncreaseHB"):
#		_calc_health(-10)
#	pass

# helper function to manage health calculations
# positive values decrease the health while negative values increase it
func _calc_health(decrease):
	if not invul:
		hit.play()
		pts.dec_multiplier()
		_health = _health - decrease
		# keeps health from going below zero and above the max
		if _health > MAX_HEALTH:
			_health = MAX_HEALTH
		elif _health < 0:
			_health = 0
		# emit signals to indicate a reset of the multiplier and / or health change
		if decrease > 0:
			# turn on invulnerability since player has taken damage
			if not _anim_player.is_playing():
				_anim_player.play("invul")
				invul = true
			self.emit_signal("multiplier_reset")
		self.emit_signal("health_change", _health)
	pass

# handles switching between controller and keyboard input
func _on_joy_connection_changed(device, connected):
	if connected:
		_jpad_connected = true
		_jpad_index = device
	else:
		_jpad_connected = false
	pass