extends RichTextLabel

onready var timer = get_node("../Timer")

var dialog = []
var line_counter = 0
var current_line = 0

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func _process(delta):
	pass
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

# Put Dialog file into Dialog array
func _start_dialog(dialogpath):
	# Fetch File
	var file = File.new()
	file.open(dialogpath, file.READ)
	
	# Read file into array
	line_counter = 0
	while !file.eof_reached():
		dialog.append("[center]" + file.get_line() + "[/center]")
		line_counter += 1
	
	file.close()


func _play_dialog():
	set_bbcode(dialog[current_line])
	set_visible_characters(0)
	timer.start()
	current_line += 1

func _empty():
	dialog.clear()
	line_counter = 0
	current_line = 0
	set_bbcode("")

func _on_Timer_timeout():
	set_visible_characters(get_visible_characters() + 1)
	pass # replace with function body
	
func _line_done():
	if get_visible_characters() >= get_total_character_count():
		return true
	else:
		return false
	
func _close_bubble():
	set_bbcode("")
