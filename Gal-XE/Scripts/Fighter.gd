extends KinematicBody2D

# container for various enumerators
const T = preload("res://Scripts/Types.gd")

# reference to navigation node that gets path to player
onready var _path_finder = self.get_node("../../World/Platforms/PlayerPathFinder")

# reference to player
onready var _player = self.get_node("../../Player")

# fighter stats
export (int, 1, 100) var MAX_HEALTH = 50
var _health = 0 setget health_set, health_get
export (int, 0, 20) var _body_attack_pow = 1 setget ,attack_get
export (int, 1000) var _point_val = 500 setget ,point_get
export (int, 100) var _mult_val = 1 setget ,mult_get

# constant values
export (int, 100) var _MAX_PULSE_CNT = 3
export (float, 2.0) var _MAX_PULSE_TM = 0.2
export (float, 1.0) var _MAX_DRIFT_TM = 0.05
var _MAX_PULSE_TRIG_TM = 0.1
export (float, 1.0, 2000) var _PULSE_ACCEL = 1000
export (float, 20.0, 30.0) var _MAX_VEL = 20.0
export var _GRAVITY = 50

# movement variables
var _pulse_vect = Vector2 (0, 0)
var _drift_vect = Vector2 (0, 0)
var _pulse_cnt = 0
var _pulse_tm = 1e20
var _drift_tm = 1e20

# movement state flags
var _mvmt_state = T.Enemy.Move.DRIFT setget ,mv_state_get

# orientation flag
var _facing_rgt = true setget ,orientation_get

# setter for _health
# value to enter is the value to change the health by
# positive values decrease the health while negative values increase it
func health_set(decrease = 0):
	self._calc_health(decrease)
	if _health == 0:
		# inform wave manager it is dead
		if self.get_parent().has_method("update_status") and not self.is_queued_for_deletion():
			self.get_parent().update_status(self)
		self.queue_free()
	pass

# getter for _health
func health_get():
	return _health
	pass

# getter for point value
func point_get():
	return _point_val
	pass

# getter for multiplier
func mult_get():
	return _mult_val
	pass

# getter for the attack poer of the body
func attack_get():
	return _body_attack_pow
	pass

# getter for orientation
func orientation_get():
	return _facing_rgt
	pass

# setter for movement vector
func mvmt_set(new_mv_vect, state = T.Enemy.Hit.NONE):
	if state != T.Enemy.Hit.BOUNCE:
		_mvmt_state = T.Enemy.Move.REACT
		_pulse_tm = 1e20
		_drift_tm = 1e20
		_drift_vect = new_mv_vect * 1.1
	pass

# getter for appropriate movement vector
func mvmt_get():
	# if pulsing the pulse vector will be greater than zero, so the pulse vector
	# is used for current movement
	if _pulse_vect.length() > 0:
		return _pulse_vect
	# if not pulsing the pulse vector is set to zero, so the drift vector is used
	# for current movement
	else:
		return _drift_vect
	pass

# getter for the state of the movement
func mv_state_get():
	return _mvmt_state
	pass

func _ready():
	# initialize fighter stats
	_health = MAX_HEALTH
	pass

func _physics_process(delta):
	match _mvmt_state:
		T.Enemy.Move.PULSE:
			# apply pulse
			if _pulse_tm < _MAX_PULSE_TM:
				_pulse_tm += delta
				self._collision_manager(_pulse_vect * _PULSE_ACCEL * delta)
			# set movement to drift
			else:
				_pulse_tm = 1e20
				_drift_tm = 0
				_drift_vect = _pulse_vect * _PULSE_ACCEL * delta
				_mvmt_state = T.Enemy.Move.DRIFT
		T.Enemy.Move.BOUNCE:
			# try to pulse if drift vector is small enough
			if abs(_drift_vect.x) < 0.1 and _pulse_cnt < _MAX_PULSE_CNT:
				_mvmt_state = T.Enemy.Move.PULSE
				self._calc_pulse_vect()
				self._collision_manager(_pulse_vect * _PULSE_ACCEL * delta)
			# go back to drift
			else:
				_mvmt_state = T.Enemy.Move.DRIFT
		T.Enemy.Move.DRIFT:
			# wait for fighter to start falling
			if _drift_vect.y < 8.0:
				self._drift_mvmt_helper(delta)
			# wait for drift timer to expire
			elif _drift_tm < _MAX_DRIFT_TM:
				_drift_tm += delta
				self._drift_mvmt_helper(delta)
			# set movement to pulse if pulse avaliable
			elif _pulse_cnt < _MAX_PULSE_CNT:
				_mvmt_state = T.Enemy.Move.PULSE
				self._calc_pulse_vect()
				self._collision_manager(_pulse_vect * _PULSE_ACCEL * delta)
			# continue falling if no pulses remain
			else:
				self._drift_mvmt_helper(delta)
		_:
			# Respond to being attacked by the player
			self._drift_mvmt_helper(delta)
	pass

# Calculates the pulse vector for the fighter based on the path from the fighter to
# the player
func _calc_pulse_vect():
	# paths from fighter to player
	var mid_path
	var edge_1_path
	var edge_2_path
	
	# calculate the middle path
	var player_pos = _player.get_global_transform().get_origin()
	var fighter_pos = self.get_global_transform().get_origin()
	mid_path = _path_finder.get_simple_path(fighter_pos, player_pos)
	
	# find the edges of the fighter
	var fighter_edges = PoolVector2Array()
	var offset = self.get_node("BodyCollision").shape.radius
	var init_vect_fight = (mid_path[1] - mid_path[0]).normalized()
	var perp_vect_fight = Vector2 ((init_vect_fight.y / init_vect_fight.x), -1).normalized()
	fighter_edges.append(fighter_pos + (offset * perp_vect_fight))
	fighter_edges.append(fighter_pos - (offset * perp_vect_fight))
	
	# find the edges of the player
	var player_edges = PoolVector2Array()
	offset = _player.get_node("BodyCollision").shape.radius
	var init_vect_player = (mid_path[mid_path.size() - 2] - mid_path[mid_path.size() - 1]).normalized()
	var perp_vect_player = Vector2 ((init_vect_player.y / init_vect_player.x), -1).normalized()
	player_edges.append(player_pos + (offset * perp_vect_player))
	player_edges.append(player_pos - (offset * perp_vect_player))
	
	# calculate the edge paths
	edge_1_path = _path_finder.get_simple_path(fighter_edges[0], player_edges[0])
	edge_2_path = _path_finder.get_simple_path(fighter_edges[1], player_edges[1])
	
	# get the node count for each path to see which paths are straight lines
	var straight_paths_num = 0
	var mid_node_cnt = mid_path.size()
	var edge_1_node_cnt = edge_1_path.size()
	var edge_2_node_cnt = edge_2_path.size()
	if mid_node_cnt <= 2:
		straight_paths_num += 1
	if edge_1_node_cnt <= 2:
		straight_paths_num += 1
	if edge_2_node_cnt <= 2:
		straight_paths_num += 1
	
	# calculate the pulse vector
	match straight_paths_num:
		# if all paths are straight lines
		3:
			_pulse_vect = (mid_path[1] - mid_path[0]).normalized()
		# if only one path is not a straight line
		2:
			# set the pulse vector to be the path that collides with an object
			if edge_1_node_cnt > 2 or edge_2_node_cnt > 2:
				if edge_1_node_cnt > 2:
					_pulse_vect = (edge_1_path[1] - edge_1_path[0]).normalized()
				else:
					_pulse_vect = (edge_2_path[1] - edge_2_path[0]).normalized()
			# set the pulse vector to be the path whose starting x coordinate is closer
			# to the x coordinate of the second node in the middle path
			else:
				var edge_1_dist = abs(mid_path[1].x - edge_1_path[0].x)
				var edge_2_dist = abs(mid_path[1].x - edge_2_path[0].x)
				if edge_1_dist > edge_2_dist:
					_pulse_vect = (mid_path[1] - edge_2_path[0]).normalized()
				else:
					_pulse_vect = (mid_path[1] - edge_1_path[0]).normalized()
		# if only two paths are not straight lines
		1:
			# fighter will try to fit through a hole it's too big for
			if mid_node_cnt <= 2:
				_pulse_vect = (mid_path[1] - mid_path[0]).normalized()
			# set the pulse vector to angle away from the paths with collisions
			elif edge_1_node_cnt <= 2:
				_pulse_vect = (mid_path[1] - edge_2_path[0]).normalized()
			# set the pulse vector to angle away from the paths with collisions
			else:
				_pulse_vect = (mid_path[1] - edge_1_path[0]).normalized()
		# if no path is a straight line
		_:
			# set the pulse vector to be the path whose starting x coordinate is closer
			# to the x coordinate of the second node in the middle path
			var edge_1_dist = abs(mid_path[1].x - edge_1_path[0].x)
			var edge_2_dist = abs(mid_path[1].x - edge_2_path[0].x)
			if edge_1_dist > edge_2_dist:
				_pulse_vect = (mid_path[1] - edge_2_path[0]).normalized()
			else:
				_pulse_vect = (mid_path[1] - edge_1_path[0]).normalized()
	
	self._pulse_helper()
	pass

# helper function to orient the player based on the direction of the pulse
func _orientation_helper():
	var prev_dir = _facing_rgt
	# get the orientation of the fighter
	if _pulse_vect.x < -0.05:
		_facing_rgt = false
	elif _pulse_vect.x > 0.05:
		_facing_rgt = true
	# flip the fighter if facing new direction than before
	if _facing_rgt != prev_dir:
		self.scale.x = -1
	pass

# helper function to set the appropriate flags and values upon recieving pulse input
func _pulse_helper():
	_pulse_tm = 0
	_pulse_cnt += 1
	_drift_tm = 1e20
	self._orientation_helper()
	pass

# helper function to handle player movement after a pulse jump
func _drift_mvmt_helper(delta):
	if _drift_vect.length() < _MAX_VEL:
		_drift_vect.y += _GRAVITY * delta
	self._collision_manager(_drift_vect)
	pass

# helper function to help with resetting _drift_tm after a collision
# if the drift time is above the threshold percentage, the drift time 
# is reset to be that percentage
func _drift_tm_reset(threshold):
	if _drift_tm > (_MAX_DRIFT_TM * threshold):
		_drift_tm = _MAX_DRIFT_TM * threshold
	pass

# helper function to handle collisions between different types of nodes
func _collision_manager(mvmt_vect):
	# get information about collision
	var collision = self.move_and_collide(mvmt_vect)
	
	if collision:
		var col_obj = collision.collider
		
		# amount to decrease the movement vector by upon bouncing off of something
		var bounce_decay
		
		# reset the pulse count after hiting a platform
		if col_obj.get_collision_layer_bit(T.Col_Layer.PLATFORM):
			_pulse_cnt = 0
		
		# take damage from player if fighter bumps into enemy
		# fighter must be moving into player for godot to register that
		# fighter bumped into player
		if col_obj.get_collision_layer_bit(T.Col_Layer.PLAYER):
			# update player health if object has the function to do so
			if col_obj.has_method("health_set"):
				col_obj.health_set(_body_attack_pow)
			"""# take damage from player if player is not being punched
			if col_obj.has_method("hit_get"):
				# check to see if player is being punched
				if col_obj.hit_get() != T.Enemy.Hit.PUNCH:
					# update player health if able to get enemy's attack power
					if col_obj.has_method("attack_get"):
						self._calc_health(col_obj.attack_get())"""
		
		# bounce fighter off of object if object can be collided into
		if not col_obj.get_collision_layer_bit(T.Col_Layer.DEFAULT):
			var reflect = collision.remainder.bounce(collision.normal)
			
			# set bounce decay for bouncing off of player
			if col_obj.get_collision_layer_bit(T.Col_Layer.PLAYER):
				# set high bounce decay if player is being punched
				if col_obj.has_method("hit_get"):
					if col_obj.hit_get() == T.Enemy.Hit.PUNCH:
						bounce_decay = 3.0
						self._drift_tm_reset(0.9)
					# set low bounce decay if player is not being punched
					else:
						bounce_decay = 1.5
						self._drift_tm_reset(0.9)
				# set default bounce decay if player is not being punched
				else:
					bounce_decay = 2.0
					self._drift_tm_reset(0.9)
			# set default bounce decay
			else:
				bounce_decay = 2.0
				self._drift_tm_reset(0.9)
			
			_drift_vect = mvmt_vect.bounce(collision.normal) / bounce_decay
			
			# stop pulse upon impact and ready next pulse
			if _mvmt_state == T.Enemy.Move.PULSE:
				_pulse_tm = 1e20
				_pulse_vect = Vector2(0, 0)
			else:
				self.move_and_collide(reflect)
			
			# set movement state to bounce
			_mvmt_state = T.Enemy.Move.BOUNCE
	pass

# helper function to manage health calculations
# positive values decrease the health while negative values increase it
func _calc_health(decrease):
	_health = _health - decrease
	# keeps health from going below zero and above the max
	if _health > MAX_HEALTH:
		_health = MAX_HEALTH
	elif _health < 0:
		_health = 0
	pass