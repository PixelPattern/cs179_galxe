extends Node

onready var ap = get_node("AnimationPlayer")
onready var bg = get_node("bganimation")
onready var op = get_node("Opening")
onready var sp = get_node("Sprite")

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var index = 0
var title = true
var check1 = false
var check2 = false
var check3 = false
var check4 = false

func _ready():
	check1 = true
	bg.play("bg")
	op.play("Spin")
	
	pass

func _process(delta):
	
	if title == true && Input.is_action_just_pressed("all"):
		op.play("none")
		title = false
		pass
	
	if op.is_playing() == false && title == true:
		op.play("wait")
	
	# Moving up and down the menu
	if title == false && sp.visible == false:
		if Input.is_action_just_pressed("ui_cancel"):
			title = true
			op.play("Spin")
		
		if Input.is_action_just_pressed("ui_up"):
			if index > 0:
				index -= 1
			else:
				index = 0
			
		if Input.is_action_just_pressed("ui_down"):
			if index < 3:
				index += 1
			else: 
				index = 3
		
		# Condition statements for animation
		if index == 0 && check1:
			ap.play("Menu1")
			check1 = false
			check2 = true
			check3 = true
			check4 = true
		if index == 1 && check2:
			ap.play("Menu2")
			check1 = true
			check2 = false
			check3 = true
			check4 = true
		if index == 2 && check3:
			ap.play("Menu3")
			check1 = true
			check2 = true
			check3 = false
			check4 = true
		if index == 3 && check4:
			ap.play("Menu4")
			check1 = true
			check2 = true
			check3 = true
			check4 = false
		
		if Input.is_action_just_pressed("ui_accept"):
			if index == 0:
				get_tree().change_scene("res://Levels/Level1.tscn")
			if index == 1:
				get_tree().change_scene("res://Levels/Level2.tscn")
			if index == 2:
				get_tree().change_scene("res://Levels/Tutorial.tscn")
			if index == 3:
				var tree = get_tree()
				tree.quit()
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
