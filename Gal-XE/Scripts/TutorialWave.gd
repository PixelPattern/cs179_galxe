extends Node

var ready = false
var done = false


onready var Narr = get_node("../Narration_Node")
onready var Player = get_node("../Player")
onready var gameover = get_node("../GameOver")

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func _process(delta):
	# check if children have been spawned
	if ready == false && get_child_count() > 0:
		ready = true
	
	if get_child_count() == 0 && done == false:
		Narr.start_dialog("res://Dialogues/Tutorial Dialog/Dialog5", "res://Sprites/Narration Assets/mechan_icon.png")
		done = true
	
	if done == true && Input.is_action_just_pressed("ui_accept"):
		get_tree().change_scene("res://Menus/TitleMenu.tscn")
		
	
	if ready == true && Player._health <= 0:
		gameover._start("res://Levels/Tutorial.tscn")
	
	
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
	pass
