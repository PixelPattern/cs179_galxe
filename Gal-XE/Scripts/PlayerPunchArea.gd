extends Area2D

# container for various enumerators
const T = preload("res://Scripts/Types.gd")

# reference to PlayerFists object
onready var _fists_node = self.get_node("../")
onready var punch_miss = get_node("punch_miss")
onready var punch_hit = get_node("punch_hit")
# amount by which enemies bounce back when punched
var _enemy_kback = Vector2 (5, -1)

func _ready():
	self.connect("body_entered", self, "_enemy_hit")
	pass

func _process(delta):
	var cur_punch_state = T.Player.Action.NO_PUNCH
	if _fists_node.has_method("punch_state_get"):
		cur_punch_state = _fists_node.punch_state_get()
	
	# activate collisions when punch state is active
	if cur_punch_state == T.Player.Action.LFT_PUNCH or cur_punch_state == T.Player.Action.RGT_PUNCH:
		if !punch_hit.is_playing():
			punch_miss.play()
		self.set_collision_mask_bit(T.Col_Layer.ENEMY, true)
		self.set_collision_mask_bit(T.Col_Layer.CHASER, true)
	
	# deactivate collisions when punch state is inert
	else:
		self.set_collision_mask_bit(T.Col_Layer.ENEMY, false)
		self.set_collision_mask_bit(T.Col_Layer.CHASER, false)
	pass

# signal function to detect enemies hit
func _enemy_hit(enemy):
	punch_hit.play()
	var kback = _enemy_kback
	# reverse knockback direction if player is facing left
	if _fists_node.get_parent().has_method("orientation_get"):
		if not self._fists_node.get_parent().orientation_get():
			kback.x = -kback.x
		
	# add player movement to knockback
	if _fists_node.get_parent().has_method("mvmt_get"):
		kback += self._fists_node.get_parent().mvmt_get()
	
	# check to see if enemy has appropriate methods for setting
	# movement and health
	if enemy.has_method("mvmt_set"):
		enemy.mvmt_set(kback, T.Enemy.Hit.PUNCH)
	if enemy.has_method("health_set"):
		enemy.health_set(1)
	pass
