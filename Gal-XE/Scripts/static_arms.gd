extends KinematicBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
const T = preload("res://Scripts/Types.gd")

onready var anim_player = get_node("AnimationPlayer")
onready var arm1 = get_node("arm1")
onready var arm2 = get_node("arm2")
onready var fist1 = get_node("arm1/fist")
onready var fist2 = get_node("arm2/fist")
func _attack(var dir):
	# Called every time the node is added to the scene.
	# Initialization here
	if !anim_player.is_playing():
		if(dir == "LEFT"):
			anim_player.play("attack_left")
		else:
			anim_player.play("attack_right")
	pass
func _flip(var dir):
	if(dir == "LEFT"):
		arm1.flip_h = false;
		arm2.flip_h = false;	
		fist1.flip_h = false;
		fist2.flip_h = false;	
		arm1.z_index = 0
		arm2.z_index = -1
		fist1.offset = Vector2(0,0)
		fist2.offset = Vector2(0,0)
	else:
		arm1.flip_h = true;
		arm2.flip_h = true;
		fist1.flip_h = true;
		fist2.flip_h = true;
		arm1.z_index = -1
		arm2.z_index = 0
		fist1.offset = Vector2(40,0)
		fist2.offset = Vector2(40,0)
		
	pass
func hit_get():
	pass
func attack_get():
	return 5
	pass
#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
