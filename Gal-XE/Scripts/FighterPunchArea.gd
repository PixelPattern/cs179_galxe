extends Area2D

# container for various enumerators
const T = preload("res://Scripts/Types.gd")

# signals
signal punch_proc
signal attack_stop

# reference to FighterFists object
onready var _fists_node = self.get_node("../")

# reference to player object
onready var _player = self.get_node("../../../../Player")

# amount by which the player bounces back when punched
var _player_kback = Vector2 (10, -5)

# variables for the reaction timer
export (float, 0.1, 1.0) var _MAX_REACT_TM = 0.3
var _react_tm = 1e20
var _reacting = false

# damage fists do
export (int, 50) var _fist_pow = 5

# flag denoting that the player is in the punch area
var _player_in_range = false

func _ready():
	self.connect("body_entered", self, "_player_enter")
	self.connect("body_exited", self, "_player_exit")
	pass

func _process(delta):
	var cur_move_state = T.Enemy.Move.DRIFT
	if _player_in_range:
		if _fists_node.get_parent().has_method("mv_state_get"):
			# intialize timer for reaction
			cur_move_state = _fists_node.get_parent().mv_state_get()
			if cur_move_state == T.Enemy.Move.REACT:
				self.emit_signal("attack_stop")
				_reacting = true
				_react_tm = 0
		if _reacting and _react_tm < _MAX_REACT_TM:
			_react_tm += delta
		elif _reacting and _react_tm >= _MAX_REACT_TM:
			if _player.has_method("is_invulnerable"):
				if not _player.is_invulnerable():
					self.emit_signal("punch_proc")
			_react_tm = 1e20
			_reacting = false
		# check to see if player is invulnerable when not reacting to an attack
		elif not _reacting:
			if _player.has_method("is_invulnerable"):
				if _player.is_invulnerable():
					self.emit_signal("attack_stop")
				else:
					self.emit_signal("punch_proc")
	else:
		_reacting = false
		_react_tm = 1e20
	
	pass

# signal function to detect when the player enters the punch area
func _player_enter(player):
	self.emit_signal("punch_proc")
	_player_in_range = true
	pass

# signal function to simulate player getting hit by the enemy fist
# connected to player_hit signal emitted by FighterFistAnimation script
# connected using the editor
func _player_hit():
	var kback = _player_kback
	# checks to see if player is invulnerable
	# if so, send signal to stop attacking
	if _player.has_method("is_invulnerable"):
		if _player.is_invulnerable():
			emit_signal("attack_stop")
		else:
			emit_signal("punch_proc")
	
	# reverse knockback direction if fighter is facing left
	if _fists_node.get_parent().has_method("orientation_get"):
		if not self._fists_node.get_parent().orientation_get():
			kback.x = -kback.x
		
	# add fighter movement to knockback
	if _fists_node.get_parent().has_method("mvmt_get"):
		kback += self._fists_node.get_parent().mvmt_get()
	
	# check to see if enemy has appropriate methods for setting
	# movement and health
	if _player.has_method("mvmt_set"):
		_player.mvmt_set(kback, T.Enemy.Hit.PUNCH)
	if _player.has_method("health_set"):
		_player.health_set(_fist_pow)
	pass

# signal function to detect when player leaves punch area
func _player_exit(player):
	self.emit_signal("attack_stop")
	_player_in_range = false
	pass