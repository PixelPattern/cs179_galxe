extends Node
onready var AP1 = get_node("AnimationPlayer") ;
onready var AP2 = get_node("AnimationPlayer2") ;
onready var AP3 = get_node("AnimationPlayer3") ;
onready var AP4 = get_node("AnimationPlayer4") ;
onready var AP5 = get_node("AnimationPlayer5") ;
# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	AP1.play("2nd to Top Traffic")
	AP2.play("planet")
	AP3.play("bottom traffic")
	AP4.play("Top Layer Traffic")
	AP5.play("3rd layer traffic")
	# Called every time the node is added to the scene.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
