extends "res://Scripts/EnemyKB.gd"

onready var left = get_node("check_left")
onready var right = get_node("check_right")
onready var wall_left = get_node("check_wall_left")
onready var wall_right = get_node("check_wall_right")
onready var animation = get_node("static_arms")
onready var player_left = get_node("hit_player_left")
onready var player_right = get_node("hit_player_right")
onready var body = get_node("Sprite")

var currDir = "LEFT"

func _charge(direction):
	# attack player if player is in the direction the enemy is facing
	# move the enemy so long as it does not hit a wall
	if direction == "LEFT" && left.is_colliding() && !wall_left.is_colliding():
		_base_mvmt = Vector2(-1, 0)
		#	currDir = "RIGHT"
		#	_base_mvmt = Vector2(2, 0)
	elif direction == "RIGHT" && right.is_colliding() && !wall_right.is_colliding():
		_base_mvmt = Vector2(1,0)
		#else:
		#	currDir = "LEFT"
		#	_base_mvmt = Vector2(-2,0)
	
	# flip the enemy around when it bumps into a wall or is about to fall off a platform
	else:
		if currDir == "LEFT":
			currDir = "RIGHT"
		else:
			currDir = "LEFT"
	
	# orient the enemy sprite so it matches the enemy movement
	if(currDir == "LEFT"):
		if(body.flip_h == true):
			if !(!left.is_colliding() and !right.is_colliding()):
				body.flip_h = false
	if(currDir == "RIGHT"):
		if(body.flip_h == false):
			if !(!left.is_colliding() and !right.is_colliding()):
				body.flip_h = true;
	pass

func _physics_process(delta):
	self._base_physics_process(delta)
	self._charge(currDir)
	if !(_hit_state == T.Enemy.Hit.GRAPPLE):
		if player_left.is_colliding() && currDir== "LEFT":
			animation._attack(currDir);
		if player_right.is_colliding() && currDir == "RIGHT":
			animation._attack(currDir);	
		if !(!left.is_colliding() and !right.is_colliding()):
			animation._flip(currDir)
	pass