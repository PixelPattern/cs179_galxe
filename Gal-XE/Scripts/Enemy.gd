extends KinematicBody2D

# container for various enumerators
const T = preload("res://Scripts/Types.gd")

# variables to indicate state of enemy
var _life_state = T.Enemy.Life.ALIVE
var _hit_state = T.Enemy.Hit.NONE
var _print_state = T.Enemy.Hit.NONE

# constant variables
export (float, 1) var _MAX_KBACK_TM = 0.1
export (float, 2) var _MAX_BOUNCE_TM = 0.5
export (int, 10) var _MAX_BOUNCE_CNT = 2
export (float, 100) var _GRAVITY = 20.0
var _MAX_FREEZE_TM = 0.1

# movement variables
var _base_mvmt = Vector2 (0, 0)
var _cur_mvmt setget mvmt_set, mvmt_get
var _bounce_cnt = 0
var _kback_tm = 1e20
var _bounce_tm = 1e20
var _freeze_tm = 1e20

# enemy stats
export (int, 100) var _max_health = 3
var _cur_health = 0 setget health_set, health_get
export (int, 1, 20) var _attack_pow = 1 setget ,attack_get
export (int, 1000) var _point_val = 100 setget ,point_get
export (int, 10) var _multiplier = 1 setget , mult_get

# setter for _mvmt_vect
func mvmt_set(new_mvmt, state = T.Enemy.Hit.NONE):
	if state != T.Enemy.Hit.BOUNCE:
		_cur_mvmt = new_mvmt
		# set the knockback timer to zero
		if state == T.Enemy.Hit.PUNCH:
			_kback_tm = 0
		_hit_state = state
	pass

# getter for _mvmt_vect
func mvmt_get():
	return _cur_mvmt
	pass

# setter for _health
# value to enter is the value to change the health by
# positive values decrease the health while negative values increase it
func health_set(decrease):
	_cur_health = _cur_health - decrease
	if _cur_health <= 0:
		_life_state = T.Enemy.Life.DEAD
	pass

# getter for _health
func health_get():
	return _cur_health
	pass

# getter for attack power
func attack_get():
	return _attack_pow
	pass

# getter for _point_val
func point_get():
	return _point_val
	pass

# gett for _multiplier
func mult_get():
	return _multiplier
	pass

# called by player to tell enemy to "freeze" when player bumps into it
# setter for _freeze
func freeze():
	print ("Enemy Frozen")
	_hit_state = T.Enemy.Hit.FREEZE
	_freeze_tm = 0
	pass

func _ready():
	_life_state = T.Enemy.Life.ALIVE
	_cur_health = _max_health
	_cur_mvmt = _base_mvmt
	pass

func _physics_process(delta):
	self._base_physics_process(delta)
	pass

# helper function to manage collisions
func _collision_manager():
	_base_collision_manager()
	pass

# base version of _physics_process function
func _base_physics_process(delta):
	if _life_state == T.Enemy.Life.ALIVE:
		# Output for testing
		if _print_state != _hit_state:
			_print_state = _hit_state
			match _print_state:
				T.Enemy.Hit.PUNCH:
					print ("Punch")
				T.Enemy.Hit.GRAPPLE:
					print ("Grapple")
				T.Enemy.Hit.BOUNCE:
					print ("Bounce")
				T.Enemy.Hit.FREEZE:
					print ("Freeze")
				T.Enemy.Hit.FALL:
					print ("Fall")
				_:
					print ("None")
		
		# Decide what to do based on the hit state of the enemy
		match _hit_state:
			T.Enemy.Hit.PUNCH:
				if _kback_tm <= _MAX_KBACK_TM:
					_kback_tm += delta
				else:
					_kback_tm = 1e20
					_hit_state = T.Enemy.Hit.FALL
			T.Enemy.Hit.GRAPPLE:
				print ("")
			T.Enemy.Hit.BOUNCE:
				if _bounce_cnt < _MAX_BOUNCE_CNT:
					_bounce_cnt += 1
				else:
					_bounce_cnt = 0
					_hit_state = T.Enemy.Hit.NONE
			T.Enemy.Hit.FREEZE:
				if _freeze_tm <= _MAX_FREEZE_TM:
					_freeze_tm += delta
				else:
					_hit_state = T.Enemy.Hit.NONE
			T.Enemy.Hit.FALL:
				_cur_mvmt.y += _GRAVITY * delta
				if _cur_mvmt.x > 0:
					_cur_mvmt.x = _cur_mvmt.x / 2
			_:
				_cur_mvmt = _base_mvmt
		self._collision_manager()
	
	else:
		# notify level/wave manager that it is dead
		self.queue_free()
	pass

# base version of helper function for managing collisions
func _base_collision_manager():
	var collision = self.move_and_collide(_cur_mvmt)
	if collision:
		var col_obj = collision.collider
		# bounce off of platforms and walls
		if col_obj.get_collision_layer_bit(T.Col_Layer.WALL) or col_obj.get_collision_layer_bit(T.Col_Layer.PLATFORM):
			if _hit_state == T.Enemy.Hit.PUNCH:
				print ("stop enemy")
			_hit_state = T.Enemy.Hit.BOUNCE
			# reset the various timers
			_kback_tm = 1e20
			_cur_mvmt = _cur_mvmt.bounce(collision.normal) / 3
			var reflect = collision.remainder.bounce(collision.normal)
			self.move_and_collide(reflect)
	pass