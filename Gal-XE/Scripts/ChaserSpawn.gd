extends Area2D

# container for various enumerators
const T = preload("res://Scripts/Types.gd")

# reference to player node
onready var _player = self.get_node("../../Player")
var _detect_player = false;

# reference to chaser scene
var _chaser_scene = load("res://Chaser.tscn")

# spawn node stats
export (float, 10, 1000) var _MAX_SPEED = 200
onready var spawn_r = self.get_node("SpawnShape").shape.radius
var spawn_pos

# variables to keep track of chasers
var _chasers = []
export (int, 1, 10) var _MAX_CHASERS = 5
var _chaser_cnt

# variable flag for if child chasers have been spawned
var rd = false

# variable for the position of the player
var player_pos

func _ready():
	self.connect("body_entered", self, "_detect_body_enter")
	self.connect("body_exited", self, "_detect_body_leave")
	spawn_pos = self.get_global_transform().get_origin()
	var x_pos
	var y_pos
	var pos_offset

	# create the chasers
	_chaser_cnt = _MAX_CHASERS
	for i in range(_chaser_cnt):
		var new_chaser = _chaser_scene.instance()
		new_chaser.set_name("chaser" + String(i))
		
		# add new chaser to spawn
		_chasers.append(new_chaser)
		self.add_child(new_chaser)
		
		# set position of new chaser
		randomize()
		x_pos = ((2 * randf()) - 1) * spawn_r
		y_pos = ((2 * randf()) - 1) * spawn_r
		
		# keep chaser within boundaries of spawn radius
		pos_offset = Vector2(x_pos, y_pos)
		if pos_offset.length() > spawn_r:
			pos_offset = pos_offset.normalized() * 0.8 * spawn_r
		new_chaser.global_position = pos_offset + spawn_pos
	pass

func _physics_process(delta):
	# Get spawn node to follow the player
	var player_pos = _player.get_global_transform().get_origin()
	spawn_pos = self.get_global_transform().get_origin()
	var mvmt_vect = (player_pos - spawn_pos).normalized()
	self.global_translate(mvmt_vect * _MAX_SPEED * delta)
	
	# check if chasers have been spawned
	if rd == false && self.get_child_count() > 0:
		rd = true
	
	# check if all chasers have been defeated
	# queue_free spawn node if so
	if _chaser_cnt <= 0:
		queue_free()
	
	_manage_chasers()
	pass

# helper function to set chasers to attack the player when the player enters the chaser
# spawn area
func _manage_chasers():
	var mvmt_vect
	var chaser_pos
	var player_pos = _player.get_global_transform().get_origin()
	var spawn_pos
	for chaser in _chasers:
		var check = weakref(chaser)
		if check.get_ref() != null and not chaser.is_queued_for_deletion():
			#var chaser_path = (check.get_ref()).get_path()
			if _detect_player && chaser.has_method("get_global_transform"):
			# && self.has_node(chaser_path):
				chaser_pos = chaser.get_global_transform().get_origin()
				if chaser.has_method("_set_attack_mode"):
					chaser._set_attack_mode(true)
				mvmt_vect = (player_pos - chaser_pos).normalized()
				chaser._set_attack_dir(mvmt_vect)
			else:
				chaser._set_attack_mode(false)
	pass

# function called when the spawn area detects a body entering its area
func _detect_body_enter(body):
	if body.get_collision_layer_bit(T.Col_Layer.PLAYER):
		_detect_player = true
	# tell chaser to resume normal movement patterns
	elif body.get_collision_layer_bit(T.Col_Layer.CHASER):
		body.out_of_bnds = false
		body.mvmt_set()
	pass

# function called when the spawn area detects a body leaving its area
func _detect_body_leave(body):
	if body.get_collision_layer_bit(T.Col_Layer.PLAYER):
		_detect_player = false
	# tell chaser to head back inside the spawn area
	elif body.get_collision_layer_bit(T.Col_Layer.CHASER):
		# get the vector for getting back to spawn
		body.out_of_bnds = true
		var chaser_pos = body.get_global_transform().get_origin()	
		var mv_to_spawn = spawn_pos - chaser_pos
		mv_to_spawn = mv_to_spawn.normalized()
		
		# do not change chaser movement if chaser is "bouncing" or being grabbed
		if body.has_method("hit_get"):
			if body.hit_get() != T.Enemy.Hit.GRAPPLE or body.hit_get() != T.Enemy.Hit.BOUNCE:
				body.mvmt_set(mv_to_spawn, T.Enemy.Hit.FALL)
	pass

# function to update the list of chasers when one is killed
# called by killed chaser
func update_status(chaser):
	_chaser_cnt = _chaser_cnt - 1
	if self.get_parent().has_method("update_status"):
		self.get_parent().update_status(chaser)
	pass

func _get_spawn_state():
	return _detect_player
	pass

func _get_player_dir():
	return _player.get_global_transform().get_origin()
	pass