# Script for a sprite that moves from the bottom of the screen to the top of the screen

extends AnimatedSprite

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	self.set_process(true)
	self.position = Vector2(self.get_viewport().size.x/2, self.get_viewport().size.y)
	pass

func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
	var cur_pos = self.position
	cur_pos.y -= delta * 200
	
#	# Wrap around screen
	if(cur_pos.y < 0):
		cur_pos.y = self.get_viewport_rect().size.y
	self.position = cur_pos
	pass
