# Tests out an implementation of "pulse jump" movement
extends RigidBody2D

# member variables and counters
var anim = ""
var lin_vect = Vector2 (0, 0)
var joy_vect = Vector2 (0, 0)
var joy_pulse_vect = Vector2 (0, 0)
var joy_device_index = 0
var pulse_cnt = 0
var pulse_trig_time = 1e20
var pulse_time = 1e20

# constant values
var JSTICK_ZERO_M = 0.25
var JSTICK_EDGE_M = 0.1
var MAX_PULSE_CNT = INF
var PULSE_TRIG_TIME = 0.2
var MAX_PULSE_TIME = 0.2
var PULSE_STR = 8000
var MAX_PULSE_VEL = 1000
var PULSE_DEACCEL = 800
var MAX_NORM_VEL = 100
var NORM_ACCEL = 50

# input state flags
var use_joy_input = true
var input_rcvd = false
var pulsing = false

# joystick state flags
var joy_centered = false

# keyboard state flags
var p_lft = false
var p_rgt = false
var p_up = false
var p_dwn = false


# Called every time the node is added to the scene
func _ready():
	# Initialization here
	Input.connect("joy_connection_changed", self, "_on_joy_connection_changed")
	pass


func _integrate_forces(state):
	lin_vect = state.get_linear_velocity()
	var step = state.get_step()
	var new_anim = anim
	
	# movement info
	var pulse_lft = false
	var pulse_rgt = false
	var pulse_up = false
	var pulse_dwn = false
	
	# get controller input
	if use_joy_input:
		joy_vect = Vector2(Input.get_joy_axis(joy_device_index, JOY_AXIS_0), Input.get_joy_axis(joy_device_index, JOY_AXIS_1))
		self._jstick_centered(step)
	# get keyboard input
	else:
		pulse_lft = Input.is_action_just_pressed("player_left")
		pulse_rgt = Input.is_action_just_pressed("player_right")
		pulse_up = Input.is_action_just_pressed("player_up")
		pulse_dwn = Input.is_action_just_pressed("player_down")
	
	# activate pulse
	if pulsing and pulse_time < MAX_PULSE_TIME:
		# controller input
		if use_joy_input:
			self._jstick_pulse(step)
		# keyboard input
		else:
			self._kboard_pulse(step)
		# update time
		pulse_time += step
	
	# deactivate pulse
	elif pulsing and pulse_time >= MAX_PULSE_TIME:
		pulsing = false
		input_rcvd = false
		pulse_time = 1e20
	
	# detect pulse input
	elif pulse_cnt < MAX_PULSE_CNT and not input_rcvd:
		if use_joy_input:
			self._jstick_input()
		else:
			self._kboard_input(pulse_lft, pulse_rgt, pulse_up, pulse_dwn)
	
	# deaccelerate player and apply gravity when not pulsing
	if not pulsing:
		# deaccelerate horizontal movement if falling down
		if lin_vect.y > 0:
			var xv = abs(lin_vect.x)
			xv -= PULSE_DEACCEL * step
			if xv < 0:
				xv = 0
			lin_vect.x = sign(lin_vect.x) * xv
		# apply gravity
		lin_vect += state.get_total_gravity() * step
	
	state.set_linear_velocity(lin_vect)
	pass


# helper function to determine pulse behaviour for keyboard controls
# adjusts the value of lin_vect variable
func _kboard_pulse(step):
	# apply horizontal pulse
	if abs(lin_vect.x) < MAX_PULSE_VEL:
		if p_lft and not p_rgt:
			lin_vect.x -= PULSE_STR * step
		elif p_rgt and not p_lft:
			lin_vect.x += PULSE_STR * step
	# apply vertical pulse
	if abs(lin_vect.y) < MAX_PULSE_VEL:
		if p_up and not p_dwn:
			lin_vect.y -= PULSE_STR * step
		elif p_dwn and not p_up:
			lin_vect.y += PULSE_STR * step
	pass


# helper function to manage pulse input for keyboard controls
# adjusts the value of lin_vect variable
func _kboard_input(pulse_lft, pulse_rgt, pulse_up, pulse_dwn):
	if pulse_lft or pulse_rgt or pulse_up or pulse_dwn:
		# determine the direction to pulse
		p_lft = pulse_lft
		p_rgt = pulse_rgt
		p_up = pulse_up
		p_dwn = pulse_dwn
		# reset linear velocity of pulse directon
		if p_lft or p_rgt:
			lin_vect.x = 0
		if p_up or p_dwn:
			lin_vect.y = 0
		pulse_time = 0
		pulsing = true
		pulse_cnt += 1
		input_rcvd = true
	pass


# helper function to manage pulse behaviour for joystick controls
# adjusts the value of lin_vect variable
func _jstick_pulse(step):
	if lin_vect.length() < MAX_PULSE_VEL:
		lin_vect += joy_pulse_vect * (PULSE_STR * step)
	pass


# helper function to manage pulse input for joystick controls
# adjusts the value of lin_vect variable
func _jstick_input():
	# if joystick magnitude is one (within margin), pulse
	if joy_centered and (joy_vect.length() + JSTICK_EDGE_M) >= 1.0:
		lin_vect = Vector2 (0, 0)
		joy_pulse_vect = joy_vect.normalized()
		pulse_time = 0
		pulse_trig_time = 1e20
		pulsing = true
		pulse_cnt += 1
		input_rcvd = true
	pass


# helper funtion to check to see if joystick is centered
# increments the pulse trigger timer
func _jstick_centered(step):
	if (joy_vect.length() - JSTICK_ZERO_M) <= 0.0:
		joy_centered = true
		pulse_trig_time = 0
	else:
		if pulse_trig_time < PULSE_TRIG_TIME:
			pulse_trig_time += step
		else:
			pulse_trig_time = 1e20
			joy_centered = false
	pass


# Handles switching between controller and keyboard input
func _on_joy_connection_changed(device, connected):
	if connected:
		use_joy_input = true
		joy_device_index = device
	else:
		use_joy_input = false
	pass
