**Gal-XE**

Gal-XE is a platformer beat-em-up game in which you play a j-pop idol piloting a small mech. You jump around in platforming levels to hunt down and beat up bosses and baddies.

---

## Game Narrative

You are Gal, an up and coming j-pop idol making a name for herself across the stars. Not all is well however, as Gal's rivl Nebula and her army of mecha stage crew seek to dominate the star charts. It is up to Gal, with her allies and her trusty mech XE, to stop Nebula's army before Nebula can take over the galaxy.

---

## Game Play

#### Objectives

The goal is to traverse throughtout the level and defeat all of the enemies in the level. If the player loses if they lose all of their health before defeating all of the enemies. The player is also aiming to get the most points possible while playing in each level.

#### Controls

The game is meant to be played using an X-Box joypad controller. The game can also be played using keyboard buttons.

###### Move

The player moves by "pulsing", which is a jump governed by a directional input as opposed to a button command. The player is able to pulse in mid-air up to a certain number of times before having to come into contact with a surface of the level.

* Controller:
	* Left Joystick: Pulse in to direction of the joystick
* Keyboard:
	* W / up-arrow: pulse up
	* A / left-arrow: pulse left
	* D / right-arrow: pulse right
	* S / down-arrow: pulse down

###### Attack

The player has two attack actions for defeating enemies: punches and grabs. Punches are the only attack action to actually deal damage to enemies. Punches have a range of what is in front of the player. Grabs move enemies in front of the player so that the player can punch them. Grabs have a sweeping range that can affect enemies above and below the player.

* Controller:
	* Left Trigger / Right Trigger: Punch
	* Left Trigger + Right Trigger: Grab Attack
* Keyboard:
	* Q / E: Punch
	* Q + E: Grab Attack

###### Heal

The player is able to recover damage at the cost of 1000 points. The heal option will not be available if the player has not accrued at least 1000 points.

* Controller:
	* B button
* Keyboard:
	* -

---

## Tools Used

Godot v 3.2.0

---

## Known Bugs

* There are currently no working executables of the game. The game can only be run throught the Godot game design software.
* If a player punches an enemy after grabbing said enemy the game will crash if that punch kills the enemy. This is usually activated on enemies that only take one hit to kill. 

---

## Resources
