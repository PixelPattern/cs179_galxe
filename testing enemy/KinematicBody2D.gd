extends KinematicBody2D

const UP = Vector2(0, -1)
var motion = Vector2()
const NUM_JUMPS = 2
var jumps = 0;
const GRAVITY = 600
func _ready():
	set_process(true)
	pass
	
func _physics_process(delta):
	motion.y += delta * GRAVITY
	
	if Input.is_action_pressed("ui_right"):
		motion.x = 200
	elif Input.is_action_pressed("ui_left"):
		motion.x = -200
	else:
		motion.x = 0
	if Input.is_action_just_pressed("ui_up") && jumps <= NUM_JUMPS:
		motion.y = -500
		jumps += 1
	if is_on_floor():
		jumps = 0;
		if Input.is_action_just_pressed("ui_up"):
			motion.y = -500
			jumps += 1
			
	motion = move_and_slide(motion, UP)
	pass

func _on_Area2D_area_entered(area):
	pass # replace with function body
