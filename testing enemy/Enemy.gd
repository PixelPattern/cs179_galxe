extends KinematicBody2D

const UP = Vector2(0, -1)
var motion = Vector2(0,0)
var died = 0;
const GRAVITY = 600
onready var target = get_parent().get_node("Player")
func _ready():
	set_process(true)
	pass

func _char_died():
	queue_free()

func _physics_process(delta):
	#motion.y += delta * GRAVITY

#	motion.x = -200
#	elif Input.is_action_pressed("ui_left"):
#		motion.x = -200
#	else:
#		motion.x = 0
#	if Input.is_action_just_pressed("ui_up") && jumps <= NUM_JUMPS:
#		motion.y = -500
#		jumps += 1
#	if is_on_floor():
#		jumps = 0;
#		if Input.is_action_just_pressed("ui_up"):
#			motion.y = -500
#			jumps += 1
	motion.x = target.position.x - self.position.x
	motion.y = target.position.y - self.position.y
	motion = move_and_slide(motion, UP)
	if get_slide_count():
		queue_free()
	pass